#!/bin/bash

# VTK-m master - May 26, 2022
VTKM_HASH=d60bcd253650a6d66551bf201d3d6a1301256ba4
VTKM_SHORT_HASH=${VTKM_HASH:0:8}

# ADIOS2 master - May 26, 2022
ADIOS_HASH=e8f5d52cd450f1edab7f3c7cc43288d5d4e4ebc9
ADIOS_SHORT_HASH=${ADIOS_HASH:0:8}

cp install_cmake.sh ubuntu18
cp install_adios2.sh ubuntu18
cp install_vtkm.sh ubuntu18
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18/
rm ubuntu18/install_adios2.sh
rm ubuntu18/install_vtkm.sh
rm ubuntu18/install_cmake.sh
docker push $IMAGE_NAME

cp install_cmake.sh ubuntu18-ompi
cp install_adios2.sh ubuntu18-ompi
cp install_vtkm.sh ubuntu18-ompi
IMAGE_NAME=kitware/vtk:ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}-ompi-latest
docker image build --build-arg VTKM_HASH=$VTKM_HASH \
  --build-arg ADIOS_HASH=$ADIOS_HASH \
  -t $IMAGE_NAME \
  ubuntu18-ompi/
rm ubuntu18-ompi/install_adios2.sh
rm ubuntu18-ompi/install_vtkm.sh
rm ubuntu18-ompi/install_cmake.sh
docker push $IMAGE_NAME

#edit names in .gitlab-ci.yml
sed -i -e "s/ci-fides-vtkm[a-zA-Z0-9]\{8\}-adios[a-zA-Z0-9]\{8\}/ci-fides-vtkm${VTKM_SHORT_HASH}-adios${ADIOS_SHORT_HASH}/g" ../../os-linux.yml
