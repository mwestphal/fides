## TODO

- Do not allow empty `.json` files to pass the schema validator.
- Incorporate the schema validation work done by Zhe Wang. - Caitlin: Partially done as validation tests, but planning to add a validation step in Fides directly
- Allow loading both files and strings in `DataSetReader`. - Caitlin: In progress
